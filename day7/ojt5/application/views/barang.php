<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- DataTable -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jq-3.3.1/dt-1.10.25/r-2.2.9/datatables.min.css"/>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jq-3.3.1/dt-1.10.25/r-2.2.9/datatables.min.js"></script>

    <!-- Sweet Alert -->
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <title>Data Barang</title>
  </head>
  <body>
  
<?= $this->session->flashdata("barang"); ?>

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
            <a class="navbar-brand" href="#">OJT 5</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                <a class="nav-item nav-link active" href="#">Data Barang</a>
                </div>
            </div>
        </div>
    </nav>


    <div class="container">
        <div class="row mt-3">
            <div class="col-md-12">

            <h3>Data Barang</h3>
            <button type="button" class="btn btn-primary mt-3 mb-3 tambahBarang" data-toggle="modal" data-target="#exampleModalCenter">
                Tambah Data
            </button>

            <!-- <form action="" method="POST">
                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group mt-3">
                            <input type="text" name="search" class="form-control" placeholder="Enter Search">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="submit">Search</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form> -->


            <?php if ($barang != []) : ?>
            <table id="tabelbarang" class="table mt-3">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Harga</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $nomer = 1; foreach ($barang as $data_perbaris) : ?>
                    <tr>
                        <th scope="row"><?= $nomer++; ?></th>
                        <td><?= $data_perbaris['nama']; ?></td>
                        <td><?= $data_perbaris['harga']; ?></td>
                        <td>
                            <a class="badge badge-info text-light ubahBarang" data-toggle="modal" data-target="#exampleModalCenter" data-id="<?= $data_perbaris['id'] ?>" style="cursor: pointer;">
                                update
                            </a>
                            <a href="<?= base_url("index.php/welcome/hapus/".$data_perbaris['id']) ?>" class="badge badge-danger" onclick="return confirm('Are You Sure ?')">delete</a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <?php else: ?>
                <h1 class="mt-3 text-danger">Data Kosong</h1>
            <?php endif; ?>

            </div>
        </div>
    </div>
  

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
    <form id="formtambahdata" action="" method="POST">
        <div class="form-group">
            <label for="nama">Nama Barang</label>
            <input type="text" class="form-control" name="nama" id="nama" placeholder="Enter Nama Barang">
        </div>
        <div class="form-group">
            <label for="harga">Harga Barang</label>
            <input type="text" class="form-control" name="harga" id="harga" placeholder="Enter Harga Barang">
        </div>
        
    </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="Submit" class="btn btn-primary">Save changes</button>
        </div>
    </form>
    </div>
  </div>
</div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script> -->

    <!-- <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<script>
$(function(){

    $("#tabelbarang").DataTable({
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "<?= base_url("index.php/welcome/ajax_barang"); ?>",
            "type": "POST"
        }
    })

    $(".tambahBarang").on("click", function(){
        $("#exampleModalLongTitle").html("Tambah Barang")
        $("#formtambahdata").prop("action", "<?= base_url("index.php/welcome/tambah"); ?>")
        $("#nama").val("")
        $("#harga").val("")
    })

    $(document).on("click", ".ubahBarang", function(e){
        e.preventDefault()
        $("#exampleModalLongTitle").html("Update Barang")
        var id = $(this).data("id")
        $.ajax({
            url: "<?= base_url('index.php/welcome/databarangbyid/') ?>"+id, 
            dataType: "JSON",
            success: function(json){
                $("#formtambahdata").prop("action", "<?= base_url("index.php/welcome/update/"); ?>"+id)
                $("#nama").val(json.nama)
                $("#harga").val(json.harga)
            }
        })
    })

    

})
</script>


  </body>
</html>