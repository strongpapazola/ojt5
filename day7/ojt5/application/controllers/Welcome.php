<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		if ($this->input->post("search")) {
			$this->db->like("nama", $this->input->post("search"));
			$this->db->or_like("harga", $this->input->post("search"));
			$query = $this->db->get("barang")->result_array();
			$data = [
				"barang" => $query
			];
		} else {
			$data = [
				"barang" => $this->db->get("barang")->result_array()
			];
		}
		$this->load->view('barang', $data);
	}

	public function ajax_barang()
	{
		$this->load->model("BarangModel");
		$show = [];
		$order_column = $this->input->post("order")[0]['column'] == 0;
		$order_dir = $this->input->post("order")[0]['dir'] == "asc";
		$i = ($order_column && $order_dir) ? $this->input->post("start") + 1 : $this->BarangModel->get_filtered_data() - $this->input->post("start");
		foreach ($this->BarangModel->datatable() as $key) {
			$data = [];
			$data[] = ($order_column && $order_dir) ? $i++ : $i--;
			$data[] = $key['nama'];
			$data[] = $key['harga'];
			$data[] = '
			<a class="badge badge-info text-light ubahBarang" data-toggle="modal" data-target="#exampleModalCenter" data-id="'. $key['id'] .'" style="cursor: pointer;">
				update
			</a>
			<a href="'. base_url("index.php/welcome/hapus/".$key['id']) .'" class="badge badge-danger" onclick="return confirm(\'Are You Sure ?\')">
				delete
			</a>
			';
			array_push($show, $data);
		}

		$data = [
			"draw" => $this->input->post("draw"),
			"data" => $show,
			"recordsFiltered" => $this->BarangModel->get_filtered_data(),
			"recordsTotal" => $this->BarangModel->get_all_data()
		];
		echo json_encode($data, JSON_PRETTY_PRINT);
		// echo json_encode($_POST, JSON_PRETTY_PRINT);
	}


	public function tambah()
	{
		$this->form_validation->set_error_delimiters('', '|');
		$this->form_validation->set_rules('nama', "Name", "required");
		$this->form_validation->set_rules('harga', "Price", "required|numeric");

		if ($this->form_validation->run() == False) {
			echo validation_errors();
		} else {
			$data = [
				"nama" => $this->input->post("nama"),
				"harga" => $this->input->post("harga")
			];
			$this->db->insert("barang", $data);
			$this->session->set_flashdata('barang', '<script>Swal.fire("Success", "Berhasil Memasukan Data", "success");</script>');
			redirect(base_url('index.php/welcome/index'));
		}
	}

	public function update($id)
	{
		$this->form_validation->set_error_delimiters('', '|');
		$this->form_validation->set_rules('nama', "Name", "required");
		$this->form_validation->set_rules('harga', "Price", "required|numeric");

		if ($this->form_validation->run() == False) {
			echo validation_errors();
		} else {
			$data = [
				"nama" => $this->input->post("nama"),
				"harga" => $this->input->post("harga")
			];
			$this->db->update("barang", $data, ['id' => $id]);
			$this->session->set_flashdata('barang', '<script>Swal.fire("Success", "Berhasil Mengubah Data '.$data['nama'].'", "success");</script>');
			redirect(base_url('index.php/welcome/index'));
		}
	}

	public function hapus($id)
	{
		$this->db->delete("barang", ['id' => $id]);
		$this->session->set_flashdata('barang', '<script>Swal.fire("Success", "Berhasil Menghapus Data", "success");</script>');
		redirect(base_url('index.php/welcome/index'));
	}

	public function databarangbyid($id)
	{
		echo json_encode($this->db->get_where("barang", ['id' => $id])->row_array());
	}
}
